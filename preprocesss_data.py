import os


def read_data(path):
    data = []
    if os.path.isdir(path):
        for file_name in os.listdir(path):
            with open(os.path.join(path, file_name), 'r') as f:
                lines = f.readlines()
                for line in lines:
                    data.append(line)

    else:
        with open(path, 'r') as f:
            lines = f.readlines()
            for line in lines:
                data.append(line)
    return data


def take_data_keyphrase(path):
    data = []

    if os.path.isdir(path):
        data_keyphrases = read_data(path + "/keywords")
        data_postag = read_data(path + "/postags")
        data_text = read_data(path + "/words")
    else:
        data_keyphrases = read_data("postag-vncore/keywords/" + path)
        data_postag = read_data("postag-vncore/postags/" + path)
        data_text = read_data("postag-vncore/words/" + path)

    for i in range(len(data_keyphrases)):
        data.append({
            "keyphrases": data_keyphrases[i],
            "postags": data_postag[i],
            "text": data_text[i]
        })
        print({
            "keyphrases": data_keyphrases[i],
            "postags": data_postag[i],
            "text": data_text[i]
        })

    return data


def build_data_train_valid_keyphrase():
    pass


if __name__ == '__main__':
    take_data_keyphrase("0-5000.txt")
